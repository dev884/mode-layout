/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mm;

import java.util.ArrayList;

/**
 *
 * @author Montoisis Marco
 */
public class ModeLine extends ArrayList<ModeComponent>
{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public int 
        Width = 0,			/// Width of this line
        Height = 0,			/// Height of this line
        ResizableCount = 0;             /// Number of resizable components

    protected int
        XBorder;

    public ModeLine(int xBorder)
    {
        this.XBorder = xBorder;
        this.Width = this.XBorder;
    }

    /**
     * This method is used to add a ModeComponent into this collection
     * it set the Height, the Width of the line
     * and the number of WRESIZABLE components
     * @param o
     * @return 
     */
    public boolean add(ModeComponent o)
    {
        this.Width += o.Component.getWidth() + this.XBorder;

        if(o.IsMode(ModeLayout.Mode.WRESIZABLE))
        {
            this.ResizableCount++;
        }

        if(o.Component.getHeight() > this.Height)
        {
            this.Height = o.Component.getHeight();
        }

        return super.add(o);
    }
}
