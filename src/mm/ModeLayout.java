/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mm;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.LayoutManager2;
import java.util.ArrayList;

/**
 *
 * @author Montoisis Marco
 */
public class ModeLayout implements LayoutManager2 {
	
    public enum Mode
    {
        UNDER, CONTINUS, WRESIZABLE
    }

    protected ArrayList<ModeLine> 
            lines = new ArrayList<>();
    protected ModeLine 
            line;
    protected ArrayList<Integer> 
            widthMask;	/// This will contains masks of line

    protected int 
            MaxWidth = 0,
            MaxHeight = 0;
    protected boolean 
            IsInit = true;

    public int
            YBorder = 5,
            XBorder = 10;

    /**
     * This method is used to add a component,
     * it manages the creation of line
     * @param component
     */
    protected void AddComponent(ModeComponent component)
    {
        if(component != null)
        {
            if((this.line == null) || (component.IsMode(Mode.UNDER)))
            {
                this.line = new ModeLine(this.XBorder);
                this.lines.add(this.line);
            }
            this.line.add(component);
        }
    }

    /**
     * This method returns a mask with the wide widths for each columns
     * @param wResize
     * @return 
     */
    protected ArrayList<Integer> GetWidths(int wResize)
    {
        ArrayList<Integer> widths = new ArrayList<>();

        /// foreach lines
        for(int i = 0; i < this.lines.size(); i++)
        {
            for(int j = 0; j < this.lines.get(i).size(); j++)
            {
                int width = 0;
                if(!this.lines.get(i).get(j).IsMode(Mode.CONTINUS))
                {
                    width = this.lines.get(i).get(j).Width;
                    
                    if(this.lines.get(i).get(j).IsMode(Mode.WRESIZABLE))
                    {
                        width += (wResize / this.lines.get(i).ResizableCount);
                    }				
                }
                if(widths.size() == j)	// If item doesn't exists
                {
                    widths.add(width);
                }
                else
                {
                    if(widths.get(j) < width)
                    {
                        widths.set(j, width);
                    }
                }
            }
        }		
        return widths;
    }

    @Override
    public void addLayoutComponent(Component component, Object modes) {
        
        Mode[] array = null;
        if(this.isArray(modes))
        {
            array = (Mode[])modes;
        }
        else
        {
            array = new Mode[]{ (Mode)modes };
        }
        
        this.AddComponent(new ModeComponent(component, (Mode[])array));
    }

    @Override
    public float getLayoutAlignmentX(Container arg0) {
        return 0.5f;
    }

    @Override
    public float getLayoutAlignmentY(Container arg0) {
        return 0.5f;
    }

    /**
     * This method set the width and the position for each controls
     * @param container 
     */
    @Override
    public void invalidateLayout(Container container) 
    {
        int topY = 0;
        int widthDifference = 0;
        if(!this.IsInit)
        {
            /**
             *  If the form has been resized, we get the difference 
             *  between the initial width and the new width
             */

            widthDifference = container.getWidth() - this.MaxWidth;
        }

        this.widthMask = this.GetWidths(widthDifference);	/// Here we get the max width for each column control

        for(int i = 0; i < this.lines.size(); i++)
        {
            ModeLine ln = this.lines.get(i);                    /// We will resize the components line by line

            if(i > 0)
            {
                topY += this.lines.get(i-1).Height;         /// Compute the top distance (add the max height of the previous control to topY)
            }

            topY += YBorder;                                    /// Add to top the default YBorder size

            if(this.IsInit)
            {
                this.MaxHeight += YBorder; 			/// Store the current height of the form
                this.MaxHeight += ln.Height;
            }

            int left = this.XBorder;				/// Begin to compute the left position
            int absoluteLeft = this.XBorder;		
            for(int j = 0; j < ln.size(); j++)
            {
                boolean CONTINUS = ln.get(j).IsMode(Mode.CONTINUS);	/// Is current control mode continus
                int controlWidth = ln.get(j).Width;	 			

                if(j > 0)
                { 
                    if(CONTINUS)
                    {
                            left += ln.get(j-1).Component.getWidth() + this.XBorder;	/// If continus, just place after the previous control
                    }
                    else
                    {			 			
                            left = absoluteLeft;										/// else place it aligned (uses the max previous column control width)
                    }
                }

                if(!this.IsInit)
                {
                    boolean WRESIZABLE = ln.get(j).IsMode(Mode.WRESIZABLE);
                    if(WRESIZABLE)	/// If the control is resizable, we can gives it a percentage of the widthDifference
                    {
                        controlWidth += (container.getWidth() - this.MaxWidth) / ln.ResizableCount;
                    }
                }

                /// Finaly, we can set the bounds of the control
                ln.get(j).Component.setBounds(left, topY, controlWidth, ln.get(j).Height);

                if(this.IsInit)
                {	/// Store the max width of the lines
                    if(this.MaxWidth < (left + controlWidth + this.XBorder))
                    {
                        this.MaxWidth = (left + controlWidth + this.XBorder);
                    }
                }

                /// Compute absoluteLeft to place the next control
                if(widthMask.get(j) > ln.get(j).Component.getWidth())
                {
                    absoluteLeft += widthMask.get(j) + XBorder;
                }
                else
                {
                    absoluteLeft += ln.get(j).Component.getWidth() + XBorder;
                }
            }
        }
        if(this.IsInit)
        {
            this.MaxHeight += this.YBorder;
        }
        this.IsInit = false;
    }

    @Override
    public Dimension maximumLayoutSize(Container arg0) {
            return new Dimension(this.MaxWidth, this.MaxHeight);
    }

    @Override
    public void addLayoutComponent(String name, Component comp) {
            // TODO Auto-generated method stub
    }

    @Override
    public void layoutContainer(Container parent) {
            // TODO Auto-generated method stub
    }

    @Override
    public Dimension minimumLayoutSize(Container parent) {
            // TODO Auto-generated method stub
            return new Dimension(this.MaxWidth,this.MaxHeight);
    }

    @Override
    public Dimension preferredLayoutSize(Container parent) {
            // TODO Auto-generated method stub
            return new Dimension(this.MaxWidth,this.MaxHeight);
    }

    @Override
    public void removeLayoutComponent(Component comp) {
            // TODO Auto-generated method stub
    }
    
    protected boolean isArray(Object obj)
    {
        return obj!=null && obj.getClass().isArray();
    }
}
