/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mm;

import java.awt.Component;
import javax.swing.JLabel;

/**
 *
 * @author Montoisis Marco
 */
public class ModeComponent 
{
    protected ModeLayout.Mode[] Modes;
    protected Component Component;
	
    protected int 
        Width,
        Height;

    public ModeComponent(Component component, ModeLayout.Mode[] modes)
    {
        this.Modes = modes;
        this.Component = component;
        this.Width = this.Component.getWidth();
        this.Height = this.Component.getHeight();
        
        if(this.Width == 0)
        {
            if(component instanceof JLabel)
            {
                this.Width = component.getPreferredSize().width;
            }
            else
            {
                this.Width = 150;
            }
        }
        if(this.Height == 0)
        {
            this.Height = 20;
        }
        
        this.Component.setSize(this.Width, this.Height);
    }

    /**
     * This method return true if this ModeComponent
     * have the given mode
     * @param mode The mode to test
     * @return true if contains the mode
     */
    protected boolean IsMode(ModeLayout.Mode mode)
    {
        if(this.Modes != null)
        {
            for(int i = 0; i < this.Modes.length; i++)
            {
                if(this.Modes[i] == mode)
                {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * This method returns the default width of the Component
     * @return
     */
    public int GetDefaultWidth()
    {
        return this.Width;
    }

    /**
     * This method returns the default height of the Component
     * @return
     */
    public int GetDefaultHeight()
    {
        return this.Height;
    }
}
